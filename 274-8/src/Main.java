/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Scanner;

/**
 *
 * @author Steven
 */
public class Main {

   public static void main(String[] Args) {
      Scanner keyboard = new Scanner(System.in);
      boolean loop = true;

      while (loop) {
         System.out.println("Please enter a phrase: ");
         String phrase = keyboard.nextLine();
         if (phrase.equalsIgnoreCase("exit")) {
            loop = false;
         }
         else if (Palindrome.isPalindrome(phrase)) {
            phrase = "\"" + phrase + "\"";
            System.out.println(phrase + " is a palindrome!");
         }
         else {
            phrase = "\"" + phrase + "\"";
            System.out.println(phrase + " is not a palindrome!");
         }
         System.out.println();
      }
   }

   private static boolean isPalindrome(String phrase) {
      throw new UnsupportedOperationException("Not supported yet."); 
      //To change body of generated methods, choose Tools | Templates.
   }
   /*
    * run:
    Please enter a phrase: 
    A Toyota's a Toyota
    "A Toyota's a Toyota" is a palindrome!

    Please enter a phrase: 
    No "X" in "Nixon"
    "No "X" in "Nixon"" is a palindrome!

    Please enter a phrase: 
    O Geronimo, no minor ego
    "O Geronimo, no minor ego" is a palindrome!

    Please enter a phrase: 
    Palindrome
    "Palindrome" is not a palindrome!

    Please enter a phrase: 
    exit

    BUILD SUCCESSFUL (total time: 1 minute 2 seconds)

    */
}
