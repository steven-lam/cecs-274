/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Steven
 */
public class Palindrome {

   public static boolean isPalindrome(String phrase) {
      // TO DO: call the overloaded isPalindrome method that takes three
      // parameters. make sure to change phrase to be all lowercase, and 
      //to give appropriate default values for startNdx and endNdx.
      return isPalindrome(phrase.toLowerCase(), 0, phrase.length() - 1);
   }

   private static boolean isPalindrome(String phrase, int startNdx, 
                                       int endNdx)
   {
      /* TO DO:
       1. set up a base case. there are two:
       a. if start == end, then you are looking at a string of length 1.
       all strings of length 1 are palindromes.
       b. if start > end, it means you were (for example) previously
       comparing index 4 to index 5, then recursed and now have
       start = 5 and end = 4. the string is still a palindrome.
       If you're clever you can combine these into a single base case.
       2. pull out the characters at the startNdx and endNdx.
       3. if both characters are LETTERS (see: Character.isLetter method),
       then compare them. if they are different, return false. if they are
       the same, recursively call isPalindrome and move start/end up/down
       by one.
       4. if either one (or both) is not a letter, recursively call
       isPalindrome and move whichever ndx is not a letter up/down by
       one.
       */
      char startChar = phrase.charAt(startNdx), 
                       endChar = phrase.charAt(endNdx);
      //Base case
      if (startNdx == endNdx || startNdx > endNdx) {
         return true;
      }
      else if (Character.isLetter(startChar) && Character.isLetter(endChar)) {
         if (Character.compare(startChar, endChar) == 0) {
            return isPalindrome(phrase, startNdx + 1, endNdx - 1);
         }
         else {
            return false;
         }
      }
      else if (!Character.isLetter(startChar) && Character.isLetter(endChar)) 
      {
         return isPalindrome(phrase, startNdx + 1, endNdx);
      }
      else if (!Character.isLetter(endChar) && Character.isLetter(startChar)) 
      {
         return isPalindrome(phrase, startNdx, endNdx - 1);
      }
      else if (!Character.isLetter(endChar) && !Character.isLetter(startChar)) 
      {
         return isPalindrome(phrase, startNdx + 1, endNdx - 1);
      }

      return false;

   }
   // TO DO: set up a NEW FILE WITH A MAIN METHOD to prompt the user to enter 
   // a phrase, then tell them if that phrase is a palindrome. repeat this 
   // process until they enter "exit" to quit.
}
