
import java.io.IOException;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Steven
 */
public class RecipeWriterTest {
   
     public static void main(String [] args ) throws IOException {
        Recipe Salmon = new Recipe (
         "Fried Salmon",
         "A tasty dinner",
         4,
         10,
         7,
         new String [] {
            "4 ounce center - cot salmon fillets",
            "2 teaspoons olive oil",
            "Kosher salt and freshly ground black pepper"
         },
         new String [] {
            "Bring salmon to room temperature 10 minutes before cooking",
            "Warm salmon with oil over low heat",
            "Season the fish with salt and pepper",
            "Cook until golden for 4 minutes",
            "Remove from pan and serve"
         }
         );
        
        RecipeWriter recipeWriter = new RecipeWriter ("Lab 7 file .txt");
        
        recipeWriter.writeRecipe(Salmon);
        recipeWriter.close();
        
        
     }
   
}
