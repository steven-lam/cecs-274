
import java.util.*;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 /**
 *
 * @author Steven Lam, cs002abs, CECS 274 M-W 3:00PM
 */

public class RecipeBook implements Iterable {

   private List myList;
   private int mCount;

   public RecipeBook() {
      myList = new ArrayList();
   }

   //method that returns the number of recipe in the book
   public int getCount() {
      return mCount;
   }

   public void add(Recipe itemToAdd) {
      myList.addLast(itemToAdd);
      mCount++;
   }

   public boolean remove(Recipe recipeRemove) {
      Object recipe = recipeRemove;
      return myList.remove(recipe);
   }

   public Recipe get(int index) {
      return (Recipe) myList.get(index);
   }

   public Recipe searchByName(String name) {
      for (Object object : myList) {
         Recipe recipeChecked = (Recipe) object;
         if (recipeChecked.getName().equals(name)) {
            return recipeChecked;
         }
      }
      return null;
   }

   public Recipe searchByIngredient(String ingredient) {
      String ingredientChecked = ingredient;
      for (Object object : myList) {
         Recipe recipeChecked = (Recipe) object;
         for (int i = 0; i < recipeChecked.getIngredientCount(); i++) {
            if (recipeChecked.hasIngredient(ingredientChecked)) {
               return recipeChecked;
            }
         }
      }
      return null;
   }

   public Iterator iterator() {
      return myList.iterator();
   }
}