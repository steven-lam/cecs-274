/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;

/**
 *
 * @author Steven
 */
public class RecipeWriter {

   private PrintStream mWriter;

   public RecipeWriter(String fileName) throws IOException {
      mWriter = new PrintStream(fileName, "UTF-8");
   }

   public void writeRecipe(Recipe recipe) {

      String ingredients = "", steps = "";
      String[] recipeIngredients = recipe.getIngredients(),
       recipeSteps = recipe.getSteps();

      for (int i = 1; i < recipeIngredients.length + 1; i++) {
         if (i == 1) {
            ingredients = recipeIngredients[0];
         }
         else {
            ingredients = ingredients + "@" + recipeIngredients[i - 1];
         }
      }

      for (int i = 1; i < recipeSteps.length + 1; i++) {
         if (i == 1) {
            steps = recipeSteps[0];
         }
         else {
            steps = steps + "@" + recipeSteps[i - 1];
         }
      }


      mWriter.println(recipe.getName() + "|" + recipe.getDescription()
       + "|" + recipe.getServings() + "|" + recipe.getPrepTime()
       + "|" + recipe.getCookTime() + "|" + ingredients + "|" + steps);
   }

   public void close() {
      mWriter.close();
   }
}
