/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Steven Lam, cs002abs, CECS 274 M-W 3:00PM
 */
public class Recipe {

   private String mRecipeName;
   private String mDescription;
   private int mServings;
   private int mPrepTime;
   private int mCookTime;
   private String[] mIngredients;
   private String[] mSteps;

   public Recipe(String recipeName, String description, int servings,
    int prepTime, int cookTime, String[] ingredients, String[] steps) {

      this.mRecipeName = recipeName;
      this.mDescription = description;
      this.mServings = servings;
      if (prepTime >= 0) {
         this.mPrepTime = prepTime;
      }
      if (cookTime >= 0) {
         this.mCookTime = cookTime;
      }
      this.mIngredients = new String[ingredients.length];
      for (int i = 0; i < ingredients.length; i++) {
         mIngredients[i] = ingredients[i] + "";
      }
      this.mSteps = steps;

   }

   public String getName() {
      return mRecipeName;
   }

   public String getDescription() {
      return mDescription;
   }

   public int getServings() {
      return mServings;
   }

   public int getPrepTime() {
      return mPrepTime;
   }

   public int getCookTime() {
      return mCookTime;
   }

   public int getIngredientCount() {
      return mIngredients.length;
   }

   public int getStepCount() {
      return mSteps.length;
   }

   public int getTotalRecipeTime() {
      int totalTime = mCookTime + mPrepTime;
      return totalTime;
   }

   public void setRecipeName(String recipeName) {
      this.mRecipeName = recipeName;
   }

   public void setDescription(String description) {
      this.mDescription = description;
   }

   public void setServings(int servings) {
      this.mServings = servings;
   }

   public void setPrepTime(int prepTime) {
      if (prepTime >= 0) {
         this.mPrepTime = prepTime;
      }
   }

   public void setCookTime(int cookTime) {
      if (cookTime >= 0) {
         this.mCookTime = cookTime;
      }
   }

   public void setIngredients(String[] ingredients) {
      this.mIngredients = new String[ingredients.length];
      System.arraycopy(ingredients, 0, this.mIngredients,
       0, ingredients.length);
   }

   public void setSteps(String[] steps) {
      this.mSteps = steps;
   }

   public String toString() {
      String masterIngredients = "", masterSteps = "", servingCheck,
                                 minutesCheck, cookCheck;
      for (int i = 0; i < mIngredients.length; i++) {
         masterIngredients = masterIngredients + "\n" + mIngredients[i];
      }
      for (int i = 0; i < mSteps.length; i++) {
         masterSteps = masterSteps + "\n" + mSteps[i];
      }
      
      servingCheck = (mServings == 1) ? " serving" : " servings";
      minutesCheck = (mPrepTime == 1) ? " minute prep, " : " minutes prep, ";
      cookCheck = (mCookTime == 1) ? " minute cooking" : " minutes cooking";
      
      String recipeOverview = mRecipeName + " \n" + mDescription + "\n"
       + mServings + servingCheck + "\n" + mPrepTime + minutesCheck
       + getCookTime() + cookCheck + "\n" + "\n"
       + "Ingredients: " + masterIngredients
       + "\n" + "\n" + "Steps: " + masterSteps + "\n";

      return recipeOverview;
   }

   public boolean hasIngredient(String Ingredient) {
      for (int i = 0; i < mIngredients.length; i++) {
         if (mIngredients[i].indexOf(Ingredient) >= 0) {
            return true;
         }
      }
      return false;
   }
   
   public String[] getIngredients() {
      return mIngredients;
   }
   
   public String[] getSteps() {
      return mSteps;
   }
}
