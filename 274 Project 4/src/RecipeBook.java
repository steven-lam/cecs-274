
import java.util.*;
import java.util.Comparator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 /**
 *
 * @author Steven Lam, cs002abs, CECS 274 M-W 3:00PM
 */

public class RecipeBook implements Iterable {

   private List mList;
   private boolean sortState = true; // new books have no recipes => sorted
   private boolean sortOrder = true; // true is ascending; false is descending
   public static final int SORT_RECIPE_NAME = 1;
   public static final int SORT_SERVINGS = 2;
   public static final int SORT_RECIPE_TIME = 3;

   public RecipeBook() {
      mList = new ArrayList();
   }

   //method that returns the number of recipe in the book
   public int getCount() {
      return mList.getCount();
   }

   public void add(Recipe itemToAdd) {
      //Add the item
      mList.addLast(itemToAdd);
      // List is always sorted if theres 2 or less items in the list
      if (mList.getCount() <= 2) {
         sortState = true;
      }
      // If there's one item in the book then the book is sorted ascending
      if (mList.getCount() == 1) {
         sortOrder = true;
      }

      if (sortState) {
         int compare;
         if (mList.getCount() > 1) {
            // Stores the last item in the list to a local variable
            Recipe lastRecipe = (Recipe) mList.get(mList.getCount() - 2);
            // compare the item about to be added to the last item in the list
            compare = itemToAdd.getName().compareTo(lastRecipe.getName());

            // When there's one item in the list, compare the item about to be
            // added to the last item to determine if the list is going to be 
            // ascending or descending
            if (mList.getCount() == 2) {
               sortOrder = compare > 0 ? true : false;
            }
            // if the list is ascending but the added item is less than 
            // the last item => List isn't sorted.
            if (mList.getCount() > 2) {
               if (sortOrder) {
                  if (compare < 0)
                     sortState = !sortState;
               }
               // if the list is descending but the added item is greater than 
               // the last item => List isn't sorted.
               else if (!sortOrder) {
                  if (compare > 0)
                     sortState = !sortState;
               }
            }
         }
      }
   }

   public boolean remove(Recipe recipeRemove) {
      Object recipe = recipeRemove;
      return mList.remove(recipe);
   }

   public Recipe get(int index) {
      return (Recipe) mList.get(index);
   }

   public Recipe searchByName(String name) {
      if (sortState) {
         System.out.println("Performing binary search");
         //               middle, left, right, ascending/descending, name
         return binarySearch(name);
      }
      else {
         System.out.println("Performing linear search");
         return linearSearch(name);
      }
   }

   private Recipe linearSearch(String name) {
      for (Object object : mList) {
         Recipe recipeChecked = (Recipe) object;
         if (recipeChecked.getName().equals(name)) {
            return recipeChecked;
         }
      }
      return null;
   }

   private Recipe binarySearch(String name) {
      if (mList.getCount() == 0) {
         return null;
      }
      else {
        // return binarySearch((mList.getCount() - 1 / 2), 0, mList.getCount() - 1, sortOrder, name);
          return binarySearch(0, mList.getCount() - 1, sortOrder, name);
      }
   }

   private Recipe binarySearch(int left, int right, boolean ascending, String name) {
      int mMiddle = (right + left) / 2;
      int mLeft = left;
      int mRight = right;
      int direction;

      Recipe binaryRecipe = (Recipe) mList.get(mMiddle);
      direction = name.compareTo(binaryRecipe.getName());
      // Flips the direction depending if the the book is ascending or 
      // descending
      if (!ascending) {
         direction *= -1;
      }
      // Base Case: null or recipe is found
      if (left > right) {
         if (mList.getCount() == 1)
         {
           if (direction == 0)
              return binaryRecipe;
         }
         return null;
      }
      if (direction == 0) {
         return binaryRecipe;
      }
      else if (direction < 0) {
         mRight = mMiddle - 1;
         return binarySearch(left, mRight, ascending, name);
      }
      else {
         mLeft = mMiddle + 1;
         return binarySearch(mLeft, right, ascending, name);
      }

   }

   // returns a list of all the recipes in the book that contains the ingredient
   // being searched for
   public List searchByIngredient(String ingredient) {
      String ingredientChecked = ingredient;
      List recipeList = new ArrayList();
      for (Object object : mList) {
         Recipe recipeChecked = (Recipe) object;
         if (recipeChecked.hasIngredient(ingredientChecked)) {
            recipeList.addLast(recipeChecked);
         }
      }
      if (recipeList.get(0) == null) {
         return null;
      }
      else {
         return recipeList;
      }
   }

   public Iterator iterator() {
      return mList.iterator();
   }

   // Sorts the recipeBook by name, # of servings or total recipe time based
   // on the integer property and ascending or descending based on the boolean
   // ascendingSort
   public void sortBook(int property, boolean ascendingSort) {
      mList.sort(new RecipeBookComparator(property, ascendingSort));
      if (property == 1) {
         sortState = true;
         sortOrder = ascendingSort ? false : true;
      }
      else {
         sortState = false;
      }
   }

   void clear() {
      mList.clear();
   }

   private class RecipeBookComparator implements Comparator {

      int mProperty;
      boolean mAscendingSort;

      public RecipeBookComparator(int property, boolean ascendingSort) {
         mProperty = property;
         mAscendingSort = ascendingSort;
      }

      public int compare(Object o1, Object o2) {
         Recipe recipeOne = (Recipe) o1, recipeTwo = (Recipe) o2;
         if (mAscendingSort) {
            switch (mProperty) {
               case SORT_RECIPE_NAME:
                  int compareRecipe = recipeOne.getName().compareToIgnoreCase(
                   recipeTwo.getName());
                  if (compareRecipe > 0) {
                     return -1;
                  }
                  else if (compareRecipe < 0) {
                     return 1;
                  }
                  return 0;
               case SORT_SERVINGS:
                  if (recipeOne.getServings() > recipeTwo.getServings()) {
                     return -1;
                  }
                  else if (recipeOne.getServings() < recipeTwo.getServings()) {
                     return 1;
                  }
                  else {
                     return 0;
                  }
               case SORT_RECIPE_TIME:
                  double recipeOneTime = recipeOne.getPrepTime()
                   + recipeOne.getCookTime(),
                   recipeTwoTime = recipeTwo.getPrepTime()
                   + recipeTwo.getCookTime();
                  if (recipeOneTime > recipeTwoTime) {
                     return -1;
                  }
                  else if (recipeOneTime < recipeTwoTime) {
                     return 1;
                  }
                  else {
                     return 0;
                  }
            }
            return 0;
         }
         else {
            switch (mProperty) {
               case SORT_RECIPE_NAME:
                  int compareRecipe = recipeOne.getName().compareToIgnoreCase(
                   recipeTwo.getName());
                  if (compareRecipe > 0) {
                     return 1;
                  }
                  else if (compareRecipe < 0) {
                     return -1;
                  }
                  return 0;
               case SORT_SERVINGS:
                  if (recipeOne.getServings() > recipeTwo.getServings()) {
                     return 1;
                  }
                  else if (recipeOne.getServings() < recipeTwo.getServings()) {
                     return -1;
                  }
                  else {
                     return 0;
                  }
               case SORT_RECIPE_TIME:
                  double recipeOneTime = recipeOne.getPrepTime()
                   + recipeOne.getCookTime(),
                   recipeTwoTime = recipeTwo.getPrepTime()
                   + recipeTwo.getCookTime();
                  if (recipeOneTime > recipeTwoTime) {
                     return 1;
                  }
                  else if (recipeOneTime < recipeTwoTime) {
                     return -1;
                  }
                  else {
                     return 0;
                  }
            }
            return 0;
         }
      }
   }
}