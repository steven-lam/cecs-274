/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Steven Lam, cs002abs, CECS 274 M-W 3:00PM
 */
public class RecipeProgram {

   public static void main(String[] args) throws IOException {
      Scanner keyboard = new Scanner(System.in);
      RecipeBook recipeBook = new RecipeBook();
      int menuChoice, recipeLoadCount = 0;
      String fileName;
      boolean exitLoop = true;
      RecipeLoader recipeLoader;
      String menu = new String(
       "Main Menu: \n" + "1. Load recipe file \n" + "2. Save recipe file \n"
       + "3. Show all recipe names \n" + "4. Show recipe details \n"
       + "5. Remove a recipe \n" + "6. Find ingredient \n" + "7. Sort recipes \n"
       + "8. Exit \n" + "Please enter a selection: \n");
      while (exitLoop) {
         String recipeSearch, recipeRemove, ingredientSearch;
         System.out.print(menu);
         while (keyboard.hasNextInt() == false) {
            System.out.println("Not a valid menu choice, try again.");
            keyboard.next();
         }
         menuChoice = keyboard.nextInt();
         keyboard.nextLine();
         switch (menuChoice) {
            case 1:
               System.out.println("Please enter the name of a recipe "
                + "database file: ");
               fileName = keyboard.nextLine();

               try {
                  recipeLoader = new RecipeLoader(fileName);
                  Recipe dummyRecipe = recipeLoader.readNextRecipe();
                  while (dummyRecipe != null) {
                     recipeBook.add(dummyRecipe);
                     recipeLoadCount++;
                     dummyRecipe = recipeLoader.readNextRecipe();
                  }
                  if (recipeLoadCount != 1) {
                     System.out.println("Loaded " + recipeLoadCount
                      + " recipes! \n");
                  }
                  else {
                     System.out.println("Loaded " + recipeLoadCount
                      + " recipe! \n");
                  }
               }
               catch (IOException e) {
                  System.out.println("Could not open the file "
                   + fileName);
               }
               recipeLoadCount = 0;
               break;
            case 2:
               int i;
               System.out.println("Please enter the name of the file to "
                + "save to: ");
               String nameFile = keyboard.next();
               RecipeWriter recipeWriter = new RecipeWriter(nameFile);
               for (i = 0; i < recipeBook.getCount(); i++) {
                  recipeWriter.writeRecipe((Recipe) recipeBook.get(i));
               }
               recipeWriter.close();
               if (i != 1) {
                  System.out.println("Saved " + i + " recipes!");
               }
               else {
                  System.out.println("Saved " + i + " recipe!");
               }
               break;
            case 3:
               System.out.println("All " + recipeBook.getCount() + " "
                + "recipes:");
               for (Object object : recipeBook) {
                  Recipe recipe = (Recipe) object;
                  System.out.println(recipe.getName());
               }
               System.out.println();
               break;
            case 4:
               System.out.println("Please enter a recipe name: ");
               recipeSearch = keyboard.nextLine();
               Recipe searchResult = recipeBook.searchByName(recipeSearch);
               if (searchResult == null) {
                  System.out.println("Could not find a recipe with that "
                   + "name \n");
               }
               else {
                  System.out.println(" \n" + "Recipe: ");
                  System.out.println(searchResult);
               }
               break;
            case 5:
               System.out.println("Please enter a recipe name: ");
               recipeRemove = keyboard.nextLine();
               if (recipeBook.remove(recipeBook.searchByName(
                recipeRemove))) {
                  System.out.println("Recipe removed \n");
               }
               else {
                  System.out.println("Could not find a recipe with "
                   + "that name \n");
               }
               break;
            case 6:
               System.out.println("Please enter an ingredient name: ");
               ingredientSearch = keyboard.nextLine();
               if (recipeBook.searchByIngredient(ingredientSearch)
                == null) {
                  System.out.println("No recipes use that "
                   + "ingredient \n");
               }
               else {
                  List recipeList = recipeBook.searchByIngredient(ingredientSearch);
                  if (recipeList.getCount() != 1) {
                     System.out.println("These recipes use that "
                      + "ingredient: ");
                  }
                  else {
                     System.out.println("This recipe uses that "
                      + "ingredient: ");
                  }
                  for (Object object : recipeList) {
                     Recipe recipe = (Recipe) object;
                     System.out.println(recipe.getName() + "\n");
                  }
               }
               break;
            case 7:
               int sortChoice,
                format;
               String[] sortField = {"name", "servings", "time"};
               String fieldName,
                orderName;
               // Checks sort field
               do {
                  System.out.println("Please choose a sort field - name (1),"
                   + " servings (2), or time (3): ");
                  while (keyboard.hasNextInt() == false) {
                     System.out.println("Please choose a sort field - name (1),"
                      + " servings (2), or time (3): ");
                     keyboard.next();
                  }
                  sortChoice = keyboard.nextInt();
               } while (sortChoice != 1 & sortChoice != 2 & sortChoice != 3);
               // Checks ascending/descending
               do {
                  System.out.println("Ascending (1) or descending (2):");
                  while (keyboard.hasNextInt() == false) {
                     System.out.println("Ascending (1) or descending (2):");
                     keyboard.next();
                  }
                  format = keyboard.nextInt();
               } while (format != 1 & format != 2);
               boolean formatChoice = (format != 2) ? false : true;
               //Sort Choice
               recipeBook.sortBook(sortChoice, formatChoice);
               // End prompt
               fieldName = sortField[sortChoice - 1];
               orderName = formatChoice ? "descending" : "ascending";
               System.out.println("Sorted recipes by " + fieldName + " in "
                + orderName + " order");
               break;
            case 8:
               exitLoop = false;
               break;
            default:
               System.out.println("That was not a valid menu option.");
               break;
         }
      }
   }
}
