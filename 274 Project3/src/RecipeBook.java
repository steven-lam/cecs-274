
import java.util.*;
import java.util.Comparator;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 /**
 *
 * @author Steven Lam, cs002abs, CECS 274 M-W 3:00PM
 */

public class RecipeBook implements Iterable {

   private List mList;
   private int mCount;
   public static final int SORT_RECIPE_NAME = 1;
   public static final int SORT_SERVINGS = 2;
   public static final int SORT_RECIPE_TIME = 3;

   public RecipeBook() {
      mList = new ArrayList();
   }

   //method that returns the number of recipe in the book
   public int getCount() {
      return mList.getCount();
   }

   public void add(Recipe itemToAdd) {
      mList.addLast(itemToAdd);
   }

   public boolean remove(Recipe recipeRemove) {
      Object recipe = recipeRemove;
      return mList.remove(recipe);
   }

   public Recipe get(int index) {
      return (Recipe) mList.get(index);
   }

   public Recipe searchByName(String name) {
      for (Object object : mList) {
         Recipe recipeChecked = (Recipe) object;
         if (recipeChecked.getName().equals(name)) {
            return recipeChecked;
         }
      }
      return null;
   }

   // returns a list of all the recipes in the book that contains the ingredient
   // being searched for
   public List searchByIngredient(String ingredient) {
      String ingredientChecked = ingredient;
      List recipeList = new ArrayList();
      for (Object object : mList) {
         Recipe recipeChecked = (Recipe) object;
         if (recipeChecked.hasIngredient(ingredientChecked)) {
            recipeList.addLast(recipeChecked);
         }
      }
      if (recipeList.get(0) == null) {
         return null;
      }
      else {
         return recipeList;
      }
   }

   public Iterator iterator() {
      return mList.iterator();
   }

   // Sorts the recipeBook by name, # of servings or total recipe time based
   // on the integer property and ascending or descending based on the boolean
   // ascendingSort
   public void sortBook(int property, boolean ascendingSort) {
      mList.sort(new RecipeBookComparator(property, ascendingSort));
   }

   void clear() {
      mList.clear();
   }

   private class RecipeBookComparator implements Comparator {

      int mProperty;
      boolean mAscendingSort;

      public RecipeBookComparator(int property, boolean ascendingSort) {
         mProperty = property;
         mAscendingSort = ascendingSort;
      }

      public int compare(Object o1, Object o2) {
         Recipe recipeOne = (Recipe) o1, recipeTwo = (Recipe) o2;
         if (mAscendingSort) {
            switch (mProperty) {
               case SORT_RECIPE_NAME:
                  int compareRecipe = recipeOne.getName().compareToIgnoreCase(
                   recipeTwo.getName());
                  if (compareRecipe > 0) {
                     return -1;
                  }
                  else if (compareRecipe < 0) {
                     return 1;
                  }
                  return 0;
               case SORT_SERVINGS:
                  if (recipeOne.getServings() > recipeTwo.getServings()) {
                     return -1;
                  }
                  else if (recipeOne.getServings() < recipeTwo.getServings()) {
                     return 1;
                  }
                  else {
                     return 0;
                  }
               case SORT_RECIPE_TIME:
                  double recipeOneTime = recipeOne.getPrepTime()
                   + recipeOne.getCookTime(),
                   recipeTwoTime = recipeTwo.getPrepTime()
                   + recipeTwo.getCookTime();
                  if (recipeOneTime > recipeTwoTime) {
                     return -1;
                  }
                  else if (recipeOneTime < recipeTwoTime) {
                     return 1;
                  }
                  else {
                     return 0;
                  }
            }
            return 0;
         }
         else {
            switch (mProperty) {
               case SORT_RECIPE_NAME:
                  int compareRecipe = recipeOne.getName().compareToIgnoreCase(
                   recipeTwo.getName());
                  if (compareRecipe > 0) {
                     return 1;
                  }
                  else if (compareRecipe < 0) {
                     return -1;
                  }
                  return 0;
               case SORT_SERVINGS:
                  if (recipeOne.getServings() > recipeTwo.getServings()) {
                     return 1;
                  }
                  else if (recipeOne.getServings() < recipeTwo.getServings()) {
                     return -1;
                  }
                  else {
                     return 0;
                  }
               case SORT_RECIPE_TIME:
                  double recipeOneTime = recipeOne.getPrepTime()
                   + recipeOne.getCookTime(),
                   recipeTwoTime = recipeTwo.getPrepTime()
                   + recipeTwo.getCookTime();
                  if (recipeOneTime > recipeTwoTime) {
                     return 1;
                  }
                  else if (recipeOneTime < recipeTwoTime) {
                     return -1;
                  }
                  else {
                     return 0;
                  }
            }
            return 0;
         }
      }
   }
}