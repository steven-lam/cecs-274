/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Random;

/**
 *
 * @author Steven Lam, cs002abs, CECS 274 M-W 3:00PM
 */
public class BinarySearchTreeTest {

   public static void main(String[] args) {
      // Constructing the tree
      BinarySearchTree tree = new BinarySearchTree();

      // Constructing Random object
      Random r = new Random();

      // Adding Integers to the List
      tree.add(3);
      tree.add(1);
      tree.add(7);
      tree.add(0);
      tree.add(2);
      tree.add(9);
      tree.add(5);

      // Prints tree's height and efficiency
      System.out.println("The Tree's Height is: " + tree.getHeight());
      System.out.println("The Tree's Efficieny is: " + tree.getEfficiency());
      tree.printInOrder();

      // Clears the tree
      tree.clear();

      // Creates a degerate tree with same values
      tree.add(0);
      tree.add(1);
      tree.add(2);
      tree.add(3);
      tree.add(5);
      tree.add(7);
      tree.add(9);

      // Prints the new tree's height and efficiency
      System.out.println("The Tree's Height is: " + tree.getHeight());
      System.out.println("The Tree's Efficieny is: " + tree.getEfficiency());
      tree.printInOrder();
      
      // Clears the tree
      tree.clear();

      // Constructs an array list
      List mList = new ArrayList();

      // Fills the array list wiht 1-100
      for (int i = 0; i < 100; i++) {
         mList.addLast(i);
      }

      // Loop used to add ints from the array list to the tree
      while (mList.getCount() != 0) {
         int index = r.nextInt(mList.getCount());
         int number = (int) mList.get(index);
         System.out.print(number + " ");
         tree.add(number);
         mList.removeAt(index);
      }
      
      
      // Prints out the height and efficiency of the new tree
      System.out.println(" \nThe Tree's Height is: " + tree.getHeight());
      System.out.println("The Tree's Efficieny is: " + tree.getEfficiency());
      
      /*
       * run:
      The Tree's Height is: 2
      The Tree's Efficieny is: 0.875
      0
      1
      2
      3
      5
      7
      9
      The Tree's Height is: 6
      The Tree's Efficieny is: 0.018229166666666668
      0
      1
      2
      3
      5
      7
      9
      92 80 45 97 47 39 53 87 62 26 61 63 17 86 99 36 28 94 6 29 41 58 74 60 
      * 49 68 82 11 27 12 52 59 69 85 15 9 31 48 13 72 93 16 64 1 77 23 4 46 
      * 51 95 54 78 83 81 42 5 43 50 84 71 18 0 33 65 76 32 56 44 55 98 20 89 
      * 3 57 38 7 90 8 14 79 70 66 34 25 21 19 96 24 30 10 88 37 73 67 91 22 
      * 75 2 35 40  
      The Tree's Height is: 12
      The Tree's Efficieny is: 0.0020345052083333335
      BUILD SUCCESSFUL (total time: 0 seconds)

       */
   }
}
